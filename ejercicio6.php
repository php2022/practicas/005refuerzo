<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $dia = 5;
        $salida = "";

        switch ($dia) {
            case 1:
                $salida = "enero";
                break;
            case 2:
                $salida = "febrero";
                break;
            case 3:
                $salida = "marzo";
                break;
            case 4:
                $salida = "abril";
                break;
            case 5:
                $salida = "mayo";
                break;
            case 6:
                $salida = "junio";
                break;
            case 7:
                $salida = "julio";
                break;
            case 8:
                $salida = "agosto";
                break;
            case 9:
                $salida = "septiembre";
                break;
            case 10:
                $salida = "octubre";
                break;
            case 11:
                $salida = "noviembre";
                break;
            case 12:
                $salida = "diciembre";
                break;
            default:
                $salida="Ese mes no existe, panoli";
                break;
        }
        echo $salida;
        ?>
    </body>
</html>
